/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.core.dictionary;

import com.kubx.icenine.util.LString;
import com.kubx.icenine.util.Listable;

/**
 *
 * @author lessardk
 */
public interface Dictionary<T extends Enum<?>> {
    
    Listable<LString> find(String code, Enum<?> topic);
    
    Iterable<Listable<LString>> list(Enum<?> topic);
}
