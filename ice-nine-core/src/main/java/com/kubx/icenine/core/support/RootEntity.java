/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.core.support;

import com.kubx.icenine.core.messaging.Event;
import org.axonframework.domain.AbstractAggregateRoot;
import org.axonframework.domain.DomainEventMessage;
import org.axonframework.domain.IdentifierFactory;
import org.axonframework.domain.MetaData;
import org.axonframework.eventsourcing.annotation.AggregateIdentifier;
import org.springframework.hateoas.Identifiable;

/**
 *
 * @author Karl
 */
public abstract class RootEntity extends AbstractAggregateRoot<String> implements Identifiable<String> {

    @AggregateIdentifier
    private final String id;
    
    protected RootEntity() {
        this.id = IdentifierFactory.getInstance().generateIdentifier();
    }
    
    protected RootEntity(String identifier) {
        this.id = identifier;
    }

    @Override
    public final String getIdentifier() {
        return id;
    }    
    
    @Override
    public final String getId() {
        return id;
    }

    protected DomainEventMessage<Event> registerEvent(Event event) {
        event.setSource(this);
        return super.registerEvent(event);
    }

    protected DomainEventMessage<Event> registerEvent(MetaData metaData, Event event) {
        event.setSource(this);
        return super.registerEvent(metaData, event);
    }
}
