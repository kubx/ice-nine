/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.core.support;

import com.kubx.icenine.core.messaging.CommandDispatcher;
import javax.annotation.PostConstruct;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.common.annotation.ParameterResolverFactory;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.repository.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Basic support class for repositories of aggregates.
 * 
 * Since aggregate root objects could not necessarily be stored or read directly from the persistence layer, having logic and extra data in it,
 * we cannot rely on Spring-data repositories directly to handle them. But for their persistent data, we can.
 * <p>
 * This is why use a two-level repository where the first one, not managed by Spring, implements a repository for aggregates by delegating the
 * persistence operation to a second repository, which handles the persistent data type instead. A {@link DataMapper} is used to map back
 * and forth the aggregate to its persistent nature.
 * 
 * @param <T> aggregate root object type
 * @param <D> persistent data type
 * @author Karl
 */
public abstract class AggregateRepository<E extends RootEntity> extends AbstractRepository<E> {
    
    @Autowired
    private EventBus eventBus;
    
    @Autowired
    private CommandBus commandBus;
    
    @Autowired
    private ParameterResolverFactory commandParameterResolverFactory;
    
    public AggregateRepository(Class<E> rootEntityType) {
        super(rootEntityType);
    }  
    
    @PostConstruct
    public void init() {        
        setEventBus(eventBus);
        
        CommandDispatcher.subscribe(getAggregateType(), this, commandBus, commandParameterResolverFactory);
    }
}
