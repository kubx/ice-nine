/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.core.dictionary;

import com.kubx.icenine.util.LString;
import com.kubx.icenine.util.Listable;
import org.springframework.data.annotation.Id;

/**
 *
 * @author lessardk
 */
public class DictionaryData implements Listable<LString> {
    
    @Id
    private DictionaryDataKey key;
    
    private LString value;

    public DictionaryDataKey getKey() {
        return key;
    }

    public void setKey(DictionaryDataKey key) {
        this.key = key;
    }

    @Override
    public LString getValue() {
        return value;
    }

    public void setValue(LString value) {
        this.value = value;
    }
    
    @Override
    public String getCode() {
        return key.getCode();
    }
}
