/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.core.messaging;

import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.annotation.AggregateAnnotationCommandHandler;
import org.axonframework.common.annotation.AbstractMessageHandler;
import org.axonframework.common.annotation.MultiParameterResolverFactory;
import org.axonframework.common.annotation.ParameterResolverFactory;
import org.axonframework.common.annotation.SimpleResourceParameterResolverFactory;
import org.axonframework.domain.AggregateRoot;
import org.axonframework.repository.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author Karl
 */
public class CommandDispatcher<A extends AggregateRoot> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandDispatcher.class);
        
    private final CommandHandler<A> aggregateCommandHandler;
    
    CommandDispatcher(Class<A> aggregateType, Repository<A> repository, ParameterResolverFactory parameterResolverFactory) {
        
        MultiParameterResolverFactory aggregateParameterResolverFactory = 
                new MultiParameterResolverFactory(parameterResolverFactory, new SimpleResourceParameterResolverFactory(this));
        
        aggregateCommandHandler = new CommandHandler<A> (
                aggregateType,
                repository,
                aggregateParameterResolverFactory);
    }
    
    public static <A extends AggregateRoot> CommandDispatcher<A> subscribe(Class<A> aggregateType, Repository<A> repository, CommandBus commandBus, ParameterResolverFactory parameterResolverFactory) {
        CommandDispatcher<A> dispatcher = new CommandDispatcher<>(aggregateType, repository, parameterResolverFactory); 

        AggregateAnnotationCommandHandler.subscribe(dispatcher.aggregateCommandHandler, commandBus);  
        
        return dispatcher;
    }

    public void dispatch(A aggregate, CommandBatch command) {
        if (!CollectionUtils.isEmpty(command.getCommands())) {
            
            command.getCommands().forEach((c) -> {
                AbstractMessageHandler handler = aggregateCommandHandler.getAnnotatedHandlerFor(c.getClass());
                if (handler != null) {
                    try {
                        handler.invoke(aggregate, GenericCommandMessage.asCommandMessage(c));
                    } catch (ReflectiveOperationException e) {
                        LOGGER.error("Fail to invoke command handler while dispatching batch, skipping", e);
                    }                    
                } else {
                    LOGGER.error("No handler found for command {} while dispatching batch, skipping", c.getClass().getName());
                }
            });
        }
    }
}
