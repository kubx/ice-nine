/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.core.messaging;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.commandhandling.annotation.AggregateAnnotationCommandHandler;
import org.axonframework.commandhandling.annotation.AggregateCommandHandlerInspector;
import org.axonframework.commandhandling.annotation.AnnotationCommandTargetResolver;
import org.axonframework.common.annotation.AbstractMessageHandler;
import org.axonframework.common.annotation.ParameterResolverFactory;
import org.axonframework.domain.AggregateRoot;
import org.axonframework.repository.Repository;

/**
 *
 * @author Karl
 */
class CommandHandler<A extends AggregateRoot> extends AggregateAnnotationCommandHandler<A> {

    // Required to make the constructor accessible
    private class AnnotationInspector extends AggregateCommandHandlerInspector<A> {

        private AnnotationInspector(Class<A> aggregateType, ParameterResolverFactory parameterResolverFactory) {
            super(aggregateType, parameterResolverFactory);
        }
    }
    
    private final Map<Class<?>, AbstractMessageHandler> annotatedHandlers;

    CommandHandler(Class<A> aggregateType, Repository<A> repository, ParameterResolverFactory parameterResolverFactory) {
        super(aggregateType, repository, new AnnotationCommandTargetResolver(), parameterResolverFactory);

        // Annoyingly, we have to inspect again our aggregate because the AggregateAnnotationCommandHandler
        // does not share its discoveries...
        AnnotationInspector inspector = new AnnotationInspector(aggregateType, parameterResolverFactory);
        
        annotatedHandlers = inspector.getHandlers().stream()
                .collect(Collectors.toMap(AbstractMessageHandler::getPayloadType, Function.identity()));
    }

    AbstractMessageHandler getAnnotatedHandlerFor(Class<?> commandClass) {
        return annotatedHandlers.get(commandClass);
    }

    @Override
    protected Object resolveReturnValue(CommandMessage<?> command, A createdAggregate) {
        return createdAggregate;
    }    
}
