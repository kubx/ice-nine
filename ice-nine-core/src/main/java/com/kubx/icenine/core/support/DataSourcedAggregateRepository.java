/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.core.support;

import org.axonframework.repository.AggregateNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.kubx.icenine.core.mapping.Mapper;

/**
 * Basic support class for repositories of aggregates.
 * 
 * Since aggregate root objects could not necessarily be stored or read directly from the persistence layer, having logic and extra data in it,
 * we cannot rely on Spring-data repositories directly to handle them. But for their persistent data, we can.
 * <p>
 * This is why use a two-level repository where the first one, not managed by Spring, implements a repository for aggregates by delegating the
 * persistence operation to a second repository, which handles the persistent data type instead. A {@link Mapper} is used to map back
 * and forth the aggregate to its persistent nature.
 * 
 * @param <T> aggregate root object type
 * @param <D> persistent data type
 * @author Karl
 */
public abstract class DataSourcedAggregateRepository<E extends RootEntity, D> extends AggregateRepository<E> {
    
    private final Mapper<E, D> dataMapper;
    
    public DataSourcedAggregateRepository(Class<E> rootEntityType, Mapper<E, D> dataMapper) {
        super(rootEntityType);
        this.dataMapper = dataMapper;
    }  

    public long count() {
        return getDataRepository().count();
    }

    public boolean exists(String id) {
        return getDataRepository().exists(id);
    }

    public E findOne(String id) {
        return map(getDataRepository().findOne(id));
    }

    public Page<E> findAll(Pageable pageable) {
        return map(getDataRepository().findAll(pageable));
    }

    public Iterable<E> findAll(Sort sort) {
        return map(getDataRepository().findAll(sort));
    }

    public Iterable<E> findAll(Iterable<String> ids) {
        return map(getDataRepository().findAll(ids));
    }

    public Iterable<E> findAll() {
        return map(getDataRepository().findAll());
    }

    protected E map(D rootData) {
        return dataMapper.map(rootData);
    }
    
    protected Iterable<E> map(Iterable<D> rootDataList) {
        return dataMapper.map(rootDataList);
    }
 
    protected Page<E> map(Page<D> dataPage) {
        return dataMapper.map(dataPage);
    }    
    
    @Override
    protected E doLoad(Object aggregateIdentifier, Long expectedVersion) {
        D data = getDataRepository().findOne(aggregateIdentifier.toString());
        if (data == null) {
            throw new AggregateNotFoundException(aggregateIdentifier, "No data found for this aggregate");
        }
        return map(data);
    }
    
    @Override
    protected void doSave(E rootEntity) {
        getDataRepository().save(dataMapper.unmap(rootEntity));
    }

    @Override
    protected void doDelete(E rootEntity) {
        getDataRepository().delete(dataMapper.unmap(rootEntity));
    }
    
    protected abstract PagingAndSortingRepository<D, String> getDataRepository();
}
