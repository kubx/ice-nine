/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.core.mapping;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;

/**
 *
 * @author Karl
 */
public class ConvertingMapper<E, D> implements Mapper<E, D>{

    private final Converter<D, E> dataToEntityConverter;
    
    private final Converter<E, D> entityToDataConverter;

    public ConvertingMapper(Converter<D, E> dataToEntityConverter, Converter<E, D> entityToDataConverter) {
        this.dataToEntityConverter = dataToEntityConverter;
        this.entityToDataConverter = entityToDataConverter;
    }    
    
    @Override
    public E map(D data) {
        return data != null ? dataToEntityConverter.convert(data) : null;
    }

    @Override
    public List<E> map(Iterable<D> dataList) {
        return StreamSupport.stream(dataList.spliterator(), false)
                .map(dataToEntityConverter::convert)
                .collect(Collectors.toList());
    }

    @Override
    public Page<E> map(Page<D> dataPage) {
        return dataPage.map(dataToEntityConverter);
    }

    @Override
    public D unmap(E entity) {
        return entity != null ? entityToDataConverter.convert(entity) : null;
    }

    @Override
    public List<D> unmap(Iterable<E> entities) {
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entityToDataConverter::convert)
                .collect(Collectors.toList());
    }    
}
