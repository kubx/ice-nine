/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.core.support;

import com.kubx.icenine.core.messaging.Command;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Karl
 */
public abstract class Controller {
    
    @Autowired
    private CommandGateway commandGateway;
    
    protected <T> T send(Object command) {
        return commandGateway.sendAndWait(command);
    }
    
    protected <T> T send(String id, Command command) {
        command.setTargetId(id);         
        return send(command);
    }    
}
