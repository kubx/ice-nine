/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.util.bean;

import com.kubx.icenine.util.bean.testmodel.AnnotatedBean;
import com.kubx.icenine.util.bean.testmodel.AssignableBean;
import com.kubx.icenine.util.bean.testmodel.CustomAnnotation;
import com.kubx.icenine.util.bean.testmodel.CustomInterface;
import com.kubx.icenine.util.bean.testmodel.SubAssignableBean;
import com.kubx.icenine.util.test.UnitTestSupport;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

/**
 *
 * @author Karl
 */
public class CustomBeanScannerTest extends UnitTestSupport {
    
    private static final String TEST_MODEL_PACKAGE = "com.kubx.icenine.util.bean.testmodel";
    
    @Mock
    private CustomBeanCandidateHandler candidateHandler;
    
    private CustomBeanScanner scanner;
    
    @Before
    public void init() {
        scanner = new CustomBeanScanner();
    }
    
    @Test
    public void testScanningForAnnotationsDetectsBeanCandidates() {
        scanner.includeAnnotations(CustomAnnotation.class);
        
        scanner.scan(TEST_MODEL_PACKAGE, candidateHandler);
        
        verify(candidateHandler).handleCustomBeanCandidate(AnnotatedBean.class);
        verify(candidateHandler, never()).handleCustomBeanCandidate(AssignableBean.class);
        verify(candidateHandler, never()).handleCustomBeanCandidate(SubAssignableBean.class);        
    }

    @Test
    public void testScanningForInterfacesDetectsBeanCandidates() {
        scanner.includeInterfaces(CustomInterface.class);
        
        scanner.scan(TEST_MODEL_PACKAGE, candidateHandler);
        
        verify(candidateHandler).handleCustomBeanCandidate(AssignableBean.class);
        verify(candidateHandler).handleCustomBeanCandidate(SubAssignableBean.class);
        verify(candidateHandler, never()).handleCustomBeanCandidate(AnnotatedBean.class);
    }

    @Test
    public void testScanningForBothAnnotationsAndInterfacesDetectsBeanCandidates() {
        scanner.includeAnnotations(CustomAnnotation.class);
        scanner.includeInterfaces(CustomInterface.class);
        
        scanner.scan(TEST_MODEL_PACKAGE, candidateHandler);
        
        verify(candidateHandler).handleCustomBeanCandidate(AssignableBean.class);
        verify(candidateHandler).handleCustomBeanCandidate(SubAssignableBean.class);
        verify(candidateHandler).handleCustomBeanCandidate(AnnotatedBean.class);
    }
}
