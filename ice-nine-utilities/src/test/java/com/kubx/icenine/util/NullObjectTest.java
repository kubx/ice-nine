/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.util;

import com.kubx.icenine.util.NullObject;
import com.kubx.icenine.util.test.UnitTestSupport;
import java.util.NoSuchElementException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Karl
 */
public class NullObjectTest extends UnitTestSupport {
    
    private class TestNullObject extends NullObject {}
    
    private TestNullObject nullObject;
    
    @Before
    public void setUp() {
        nullObject = new TestNullObject();
    }
    
    @Test
    public void isNullIsTrueByDefault() {
        assertTrue(nullObject.isNull());
    }
    
    @Test
    public void toOptionalReturnsEmptyOptional() {
        assertFalse(nullObject.toOptional().isPresent());
        try {
            nullObject.toOptional().get();
            fail();
        } catch (NoSuchElementException e) {}
    }
}
