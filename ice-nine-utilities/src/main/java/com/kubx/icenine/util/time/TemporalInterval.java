/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.util.time;

import java.time.temporal.Temporal;

/**
 *
 * @author Karl
 */
abstract class TemporalInterval<T extends Temporal> {
    
    private T start;
    
    private T end;
    
    protected TemporalInterval(T start, T end) {
        this.start = start;
        this.end = end;
    }
    
    public T getStart() {
        return start;
    }
    
    public T getEnd() {
        return end;
    }
    
    public boolean isStartDefinite() {
        return start != null;
    }
    
    public boolean isEndDefinite() {
        return end != null;
    }
    
    public boolean isDefinite() {
        return isStartDefinite() && isEndDefinite();
    }
}
