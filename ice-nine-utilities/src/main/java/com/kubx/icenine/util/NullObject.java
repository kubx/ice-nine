/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.util;

import java.util.Optional;

/**
 * An implementation of a {@link Nullable} for nulled objects.
 * 
 * When a null-value object should be return to a caller, this class could be extended to inherit
 * default behavior of a nulled object. For example, 
 * <pre>
 * {@code
 * NullObject object = new NullObject();
 * 
 * assertTrue(object.isNull());
 * assertFalse(object.toOptional().isPresent());
 * }
 * </pre>
 * 
 * @author Karl
 */
public class NullObject implements Nullable {

    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public <T extends Nullable> Optional<T> toOptional() {
        return Optional.empty();
    }
}
