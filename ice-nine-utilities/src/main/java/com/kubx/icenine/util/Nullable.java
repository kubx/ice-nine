/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.util;

import java.util.Optional;

/**
 * Mark an object type as nullable.
 * 
 * A nullable object is useful to avoid if (null) then... checks every time a value is retrieved by a caller. 
 * <p>
 * It is an alternative to the {@link Optional} approach where a returned value must be casted and wrapped while, in this case,
 * it is not required. The caller can always assume that the returned value is not null and is of the desired type. However, if
 * a "null" value shall be returned, the object could return empty data.
 * <p>
 * The correct way use it would be have an interface extending this one, and defaulting all its method to a "empty" returned value.
 * The "null" implementation of this interface could then only implement it in addition of extending the superclass {@link NullObject}.
 * <p>
 * For example, 
 * <pre>
 * <code>
 * public interface Patata extends Nullable {
 *      default int getTubercleCount() {
 *          return 0;
 *      }
 * } 
 * 
 * public class PatataImpl implements Patata {
 *      {@literal @}Override
 *      int getTubercleCount() {
 *          return 10;
 *      }
 * }
 * 
 * public class NullPatataImpl extends NullObject implements Patata {}
 * 
 * assertFalse(new PatataImpl().isNull());
 * assertEquals(10, new PatataImpl().getTubercleCount());
 * 
 * assertTrue(new NullPatataImpl().isNull());
 * assertEquals(0, new NullPatataImpl().getTubercleCount());
 * </code>
 * </pre>
 * 
 * @author Karl
 */
public interface Nullable {

    /**
     * Check explictly if this instance is a null-value object or not.
     * 
     * @return true if this instance is a null-value object
     */
    default boolean isNull() {
        return false;
    }
    
    /**
     * Return this object in a {@link Optional} nature.
     * 
     * If this instance if a null-value object, the optional will be returned empty. Otherwise, it will contain a reference
     * to this same object. This allows so the caller to do whatever is cool with optional objects.
     * 
     * @param <T> type of the class inheriting this interface, for easy casting
     * @return an optional
     */
    default <T extends Nullable> Optional<T> toOptional() {
        return (Optional<T>) Optional.of(this);
    }
}
