/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.util.exception;

/**
 * An exception wrapper that could be used to wrap a normal exception in a runtime exception.
 * 
 * By catching a wrapped checked exception instead of a runtime one, the caller might know if it
 * has to deal with a known or unknown case of error.
 * 
 * @author Karl
 */
public class WrappedCheckedException extends RuntimeException {

    public WrappedCheckedException() {
    }

    public WrappedCheckedException(String message) {
        super(message);
    }

    public WrappedCheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrappedCheckedException(Throwable cause) {
        super(cause);
    }
    
}
