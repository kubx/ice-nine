/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.util;

import java.util.Optional;
import java.util.function.Supplier;

/**
 *
 * @author Karl
 */
public class Lazy<T> {
    
    private Optional<T> value;
    
    public T get(Supplier<T> supplier) {
        initValue(supplier);
        return value.orElse(null);
    }

    public Optional<T> getOptional(Supplier<T> supplier) {
        initValue(supplier);
        return value;
    }
    
    public void set(T value) {
        this.value = Optional.ofNullable(value);
    }
    
    public boolean isInitialized() {
        return value != null;
    }
    
    private void initValue(Supplier<T> supplier) {
        if (value == null) {
            value = Optional.ofNullable(supplier.get());
        }        
    }
}
