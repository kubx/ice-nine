/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.util;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 *
 * @author Karl
 */
public class LString {
    
    private Map<String, String> translations;

    public LString() {
        this.translations = new HashMap<>();
    }
    
    public LString(String value) {
        this();
        set(value);
    }

    public LString(Map<String, String> translations) {
        this.translations = translations;
    }
    
    public void set(String value) {
        setIn(value, getCurrentLocale());
    }

    public void setIn(String value, Locale locale) {
        setIn(locale.getLanguage(), value);
    }
    
    public void setIn(String value, String language) {
        translations.put(language, value);
    }
    
    public String get() {
        return getIn(getCurrentLocale());
    }
    
    public String getIn(Locale locale) {
        return getIn(locale.getLanguage());
    }
    
    public String getIn(String language) {
        return translations.get(language);
    }
    
    public Map<String, String> getTranslations() {
        return translations;
    }
    
    public boolean isAvailableIn(Locale locale) {
        return translations.containsKey(locale.getLanguage());
    }
    
    public void merge(LString source) {
        for (Map.Entry<String, String> translation : source.translations.entrySet()) {
            translations.put(translation.getKey(), translation.getValue());
        }
    }

    @Override
    public String toString() {
        String value = translations.get(getCurrentLocale().getLanguage());
        
        if (value == null && !translations.isEmpty()) {
            value = translations.values().iterator().next();
        }
        return value;
    }
        
    private Locale getCurrentLocale() {
        return LocaleContextHolder.getLocale();
    }
}
