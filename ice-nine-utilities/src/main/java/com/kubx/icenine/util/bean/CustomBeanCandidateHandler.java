/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.util.bean;

/**
 * A callback function to be invoked when a custom bean has been detected during a scan using {@link CustomBeanScanner}
 * 
 * @author Karl
 */
@FunctionalInterface
public interface CustomBeanCandidateHandler {
    
    /**
     * Handle a bean detected during a scan.
     * 
     * The callback might do further checks to decide if this bean should be registered and if positively, should keep 
     * a reference of that bean for further uses.
     * 
     * @param beanClass class that has passed successfully all scan criteria
     */
    void handleCustomBeanCandidate(Class<?> beanClass);
}
