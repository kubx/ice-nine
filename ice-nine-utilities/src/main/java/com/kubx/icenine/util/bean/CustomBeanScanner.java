/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.util.bean;

import com.kubx.icenine.util.exception.WrappedCheckedException;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;

/**
 * Class that scans packages for class on inclusion filters.
 * 
 * Such classes should not be Spring-compatible beans, which are handled by the Spring framework. They should
 * be custom resources that are handled directly by the application.
 * 
 * @author Karl
 */
public class CustomBeanScanner {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomBeanScanner.class);

    private final ClassPathScanningCandidateComponentProvider scanProvider;

    public CustomBeanScanner() {
        scanProvider = new ClassPathScanningCandidateComponentProvider(false);    
    }
    
    /**
     * Include a list of annotations for filtering.
     * 
     * Any classes annotated with at least one of the given annotations will be detected as a custom bean candidate.
     * 
     * @param annotationTypes list of annotations to detect
     */
    @SafeVarargs
    public final void includeAnnotations(Class<? extends Annotation>... annotationTypes) {
        
        Arrays.asList(annotationTypes).stream()
                .map(type -> new AnnotationTypeFilter(type))
                .forEach(scanProvider::addIncludeFilter);
    }

    /**
     * Include a list of interfaces for filtering.
     * 
     * Any classes implementing at least one of the given interface, directly or indirectly, will be detected as a custom bean candidate.
     * 
     * @param interfaceTypes list of interfaces to detect
     */
    @SafeVarargs
    public final void includeInterfaces(Class<?>... interfaceTypes) {
        
        Arrays.asList(interfaceTypes).stream()
                .map(type -> new AssignableTypeFilter(type))
                .forEach(scanProvider::addIncludeFilter);
    }
    
    /**
     * Scan a package for beans.
     *
     * Only classes passing inclusion filters passed in the constructor are candidate to be a custom bean.
     *
     * @param packageName name of the package to scan
     * @param handler handler of bean candidates
     * @throws WrappedCheckedException wrapped ClassNotFoundException if bean classes cannot be resolved
     */
    public void scan(String packageName, CustomBeanCandidateHandler handler) {
        
        LOGGER.info("Scanning package \"{}\" for resources", packageName);
        
        scanProvider.findCandidateComponents(packageName).stream()
                .map(this::resolveBeanClass)
                .forEach(handler::handleCustomBeanCandidate);
    }
    
    private Class<?> resolveBeanClass(BeanDefinition beanDefinition) {
        try {
            LOGGER.trace("{} is detected as a bean candidate", beanDefinition.getBeanClassName());
            return ((AbstractBeanDefinition) beanDefinition).resolveBeanClass(getClass().getClassLoader());
            
        } catch (ClassNotFoundException e) {
            LOGGER.error("{} class cannot be loaded, bean is discarded", beanDefinition.getBeanClassName());
            throw new WrappedCheckedException(e);
        }
    }
}
