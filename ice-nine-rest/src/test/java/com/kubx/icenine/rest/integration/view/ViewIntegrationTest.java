/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.integration.view;

import com.kubx.icenine.rest.integration.view.controller.BobsJaxRsResource;
import com.kubx.icenine.rest.test.IntegrationTestSupport;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

/**
 *
 * @author Karl
 */
@ContextConfiguration(classes = ViewIntegrationTestConfiguration.class)
public class ViewIntegrationTest extends IntegrationTestSupport {

    public static final String TEST_NAME = "this is a putput test";

    @Override
    protected ResourceConfig configure() {
        return super.configure().register(BobsJaxRsResource.class);
    }

    @Test
    public void testViewSerializationUsingOutboundView() {
        final JsonObject response = readJson(target("test/bob").request().get());

        Assert.assertTrue(response.containsKey("visibleName"));
        Assert.assertEquals(TEST_NAME.toUpperCase(), response.getString("visibleName"));
    }

    @Test
    public void testViewDeserializationUsingInboundView() {
        JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
        jsonBuilder.add("visibleName", TEST_NAME.toUpperCase());

        target("test/bob").request().post(writeJson(jsonBuilder.build()));
    }

    @Test
    public void testViewSerializationUsingIOBoundView() {
        final JsonObject response = readJson(target("test/bobby").request().get());

        Assert.assertTrue(response.containsKey("visibleName"));
        Assert.assertEquals(TEST_NAME.toUpperCase(), response.getString("visibleName"));
    }

    @Test
    public void testViewDeserializationUsingIOBoundView() {
        JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
        jsonBuilder.add("visibleName", TEST_NAME.toUpperCase());

        target("test/bobby").request().post(writeJson(jsonBuilder.build()));
    }
}
