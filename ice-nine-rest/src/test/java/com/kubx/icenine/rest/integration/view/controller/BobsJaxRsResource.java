/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.integration.view.controller;

import com.kubx.icenine.rest.integration.view.ViewIntegrationTest;
import com.kubx.icenine.rest.integration.view.model.BobBO;
import com.kubx.icenine.rest.integration.view.model.BobbyBO;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.junit.Assert;

/**
 *
 * @author Karl
 */
@Path(value = "test")
public class BobsJaxRsResource {
    
    @GET
    @Path(value = "bob")
    @Produces(value = "application/json")
    public BobBO getBob() {
        return new BobBO(ViewIntegrationTest.TEST_NAME);
    }

    @POST
    @Path(value = "bob")
    @Consumes(value = "application/json")
    public void postBob(BobBO test) {
        Assert.assertNotNull(test);
        Assert.assertEquals(ViewIntegrationTest.TEST_NAME.toLowerCase(), test.getPersistentName());
    }

    @GET
    @Path(value = "bobby")
    @Produces(value = "application/json")
    public BobbyBO getBobby() {
        return new BobbyBO(ViewIntegrationTest.TEST_NAME);
    }

    @POST
    @Path(value = "bobby")
    @Consumes(value = "application/json")
    public void postBobby(BobbyBO test) {
        Assert.assertNotNull(test);
        Assert.assertEquals(ViewIntegrationTest.TEST_NAME.toLowerCase(), test.getPersistentName());
    }
    
}
