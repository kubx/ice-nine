/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.integration.jsonview.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.kubx.icenine.rest.integration.jsonview.model.Person;
import com.kubx.icenine.rest.module.ser.level.DetailedLevel;
import com.kubx.icenine.rest.module.ser.level.MinimalLevel;
import com.kubx.icenine.rest.module.ser.level.SummaryLevel;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 *
 * @author Karl
 */
@Path(value = "test")
public class JsonViewJaxRsResource {
    
    @GET
    @Path(value = "inactive")
    @Produces(value = "application/json")
    public Person getNomna() {
        return new Person();
    }

    @GET
    @Path(value = "minimal")
    @Produces(value = "application/json")
    @JsonView(value = MinimalLevel.class)
    public Person getMinimalNomna() {
        return new Person();
    }

    @GET
    @Path(value = "summary")
    @Produces(value = "application/json")
    @JsonView(value = SummaryLevel.class)
    public Person getNomnaSummary() {
        return new Person();
    }

    @GET
    @Path(value = "detailed")
    @Produces(value = "application/json")
    @JsonView(value = DetailedLevel.class)
    public Person getNomnaDetails() {
        return new Person();
    }
    
}
