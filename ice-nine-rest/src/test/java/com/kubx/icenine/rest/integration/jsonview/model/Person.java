/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.integration.jsonview.model;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kubx.icenine.rest.integration.jsonview.StubSerializer;
import com.kubx.icenine.rest.module.annotation.UseJsonView;
import com.kubx.icenine.rest.module.ser.level.DetailedLevel;
import com.kubx.icenine.rest.module.ser.level.MinimalLevel;
import com.kubx.icenine.rest.module.ser.level.SummaryLevel;

/**
 *
 * @author Karl
 */
public class Person {

    public String getName() {
        return "Nouna";
    }

    @JsonView(SummaryLevel.class)
    public String getNickname() {
        return "Nomna";
    }

    @JsonView(DetailedLevel.class)
    public String getRealName() {
        return "Noam";
    }
    
    @JsonView(DetailedLevel.class)
    @UseJsonView(SummaryLevel.class)
    public Person getSummary() {
        return this;
    }
    
    @JsonView(DetailedLevel.class)
    @UseJsonView(value = MinimalLevel.class, activeOnly = true)
    public Address getAddress() {
        return new Address();
    }
}
