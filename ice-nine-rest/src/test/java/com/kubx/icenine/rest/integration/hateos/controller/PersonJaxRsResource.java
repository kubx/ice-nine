/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.integration.hateos.controller;

import com.kubx.icenine.rest.integration.hateos.model.Person;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.stereotype.Component;

/**
 *
 * @author Karl
 */
@Component
@Path("person")
@Produces(MediaType.APPLICATION_JSON)
@ExposesResourceFor(Person.class)
public class PersonJaxRsResource {

    @GET
    @Path("{id}")
    public Person getPerson(@PathParam("id") Long id) {
        return new Person(id, "yo");
    }
}
