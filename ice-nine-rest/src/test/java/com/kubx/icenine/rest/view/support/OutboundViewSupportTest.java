/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view.support;

import com.kubx.icenine.rest.test.UnitTestSupport;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.hateoas.EntityLinks;

/**
 *
 * @author Karl
 */
public class OutboundViewSupportTest extends UnitTestSupport {
    
    private static class TestView<Object> extends OutboundViewSupport {}
    
    @Mock
    private EntityLinks entityLinks;
    
    private TestView view;
    
    @Before
    public void init() {
        view = new TestView();
    }
    
    @Test
    public void testAdapteeIsNullAtStart() {
        Assert.assertNull(view.getAdaptee());
    }
    
    @Test
    public void testAdapteeIsSetAfterPrepareForSerializationCalled() {
        Object value = new Object();
        view.prepareForSerialization(value, entityLinks);
        
        Assert.assertNotNull(view.getAdaptee());
        Assert.assertSame(value, view.getAdaptee());
    }
}
