/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.integration.view;

import com.kubx.icenine.rest.integration.view.controller.BobsJaxRsResource;
import com.kubx.icenine.rest.view.registration.ViewRegistrar;
import com.kubx.icenine.rest.view.registration.ViewRegistration;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.kubx.icenine.rest.config.EnableIceNineRestSupport;

/**
 *
 * @author Karl
 */
@Configuration
@ComponentScan("com.kubx.icenine.rest.integration.view")
@EnableIceNineRestSupport
public class ViewIntegrationTestConfiguration extends ResourceConfig {
    
    public ViewIntegrationTestConfiguration() {
        packages("com.kubx.icenine.rest.integration.view");
        register(BobsJaxRsResource.class);
    }
    @Bean
    public ViewRegistration viewRegistration(ViewRegistrar viewRegistrar) {
        return viewRegistrar.registerPackage("com.kubx.icenine.rest.integration.view.model").done();
    }
}
