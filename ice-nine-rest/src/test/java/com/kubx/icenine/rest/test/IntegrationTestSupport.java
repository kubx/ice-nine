/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.icenine.rest.test;

import com.kubx.icenine.rest.config.JaxRsObjectMapperProvider;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author Karl
 */
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class IntegrationTestSupport extends JerseyTest {

    private MockHttpServletRequest httpRequest;
    
    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        httpRequest = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(httpRequest));
    }

    @Override
    protected ResourceConfig configure() {
        return new ResourceConfig(JacksonFeature.class, JaxRsObjectMapperProvider.class)
                .property("contextConfigLocation", "classpath:META-INF/integrationContext.xml");
    }
    
    protected JsonObject readJson(Response response) {
        JsonReader reader = Json.createReader((InputStream) response.getEntity());
        return reader.readObject();
    }
    
    protected Entity<String> writeJson(JsonObject json) {
        return Entity.json(json.toString());
    }
}
