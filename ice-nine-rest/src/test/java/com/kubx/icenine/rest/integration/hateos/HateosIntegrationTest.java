/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.integration.hateos;

import com.kubx.icenine.rest.integration.hateos.controller.PersonJaxRsResource;
import com.kubx.icenine.rest.test.IntegrationTestSupport;
import javax.json.JsonObject;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

/**
 *
 * @author Karl
 */
@ContextConfiguration(classes = { HateosIntegrationTestConfiguration.class, PersonJaxRsResource.class })
public class HateosIntegrationTest extends IntegrationTestSupport {

    @Override
    protected ResourceConfig configure() {
        return super.configure().register(PersonJaxRsResource.class);
    }

    @Test
    public void testSelfLink() {
        final Long id = 10L;
        final JsonObject response = readJson(target("person/" + id).request().get());
        
        System.out.println(response);
    }
}
