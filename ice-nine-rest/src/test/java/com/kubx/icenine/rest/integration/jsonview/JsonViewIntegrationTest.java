/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.integration.jsonview;

import com.kubx.icenine.rest.config.IceNineRestConfiguration;
import com.kubx.icenine.rest.integration.jsonview.controller.JsonViewJaxRsResource;
import com.kubx.icenine.rest.test.IntegrationTestSupport;
import javax.json.JsonObject;
import org.glassfish.jersey.server.ResourceConfig;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

/**
 *
 * @author Karl
 */
@ContextConfiguration(classes = IceNineRestConfiguration.class)
public class JsonViewIntegrationTest extends IntegrationTestSupport {   
    
    @Override
    protected ResourceConfig configure() {
        return super.configure().register(JsonViewJaxRsResource.class);
    }

    @Test
    public void testInactiveJsonView() {
        final JsonObject response = readJson(target("test/inactive").request().get());

        assertTrue(response.containsKey("name"));
        assertTrue(response.containsKey("nickname"));
        assertTrue(response.containsKey("realName"));
        assertTrue(response.containsKey("summary"));
        
        assertTrue(response.getJsonObject("summary").containsKey("name"));
        assertTrue(response.getJsonObject("summary").containsKey("nickname"));
        assertFalse(response.getJsonObject("summary").containsKey("realName"));
        
        assertTrue(response.containsKey("address"));
        assertTrue(response.getJsonObject("address").containsKey("civicNo"));
        assertTrue(response.getJsonObject("address").containsKey("street"));
    }

    @Test
    public void testMinimalJsonView() {
        final JsonObject response = readJson(target("test/minimal").request().get());

        assertTrue(response.containsKey("name"));
        assertFalse(response.containsKey("nickname"));
        assertFalse(response.containsKey("realName"));
        assertFalse(response.containsKey("summary"));
    }

    @Test
    public void testSummaryJsonView() {
        final JsonObject response = readJson(target("test/summary").request().get());

        assertTrue(response.containsKey("name"));
        assertTrue(response.containsKey("nickname"));
        assertFalse(response.containsKey("realName"));
        assertFalse(response.containsKey("summary"));
    }

    @Test
    public void testDetailedJsonView() {
        final JsonObject response = readJson(target("test/detailed").request().get());

        assertTrue(response.containsKey("name"));
        assertTrue(response.containsKey("nickname"));
        assertTrue(response.containsKey("realName"));
        assertTrue(response.containsKey("summary"));
        
        assertTrue(response.getJsonObject("summary").containsKey("name"));
        assertTrue(response.getJsonObject("summary").containsKey("nickname"));
        assertFalse(response.getJsonObject("summary").containsKey("realName"));
        
        assertTrue(response.containsKey("address"));
        assertTrue(response.getJsonObject("address").containsKey("civicNo"));
        assertFalse(response.getJsonObject("address").containsKey("street"));
    }
}
