/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.module.annotation;

import com.fasterxml.jackson.annotation.JsonView;
import com.kubx.icenine.rest.module.ser.level.DetailedLevel;
import com.kubx.icenine.rest.module.ser.level.MinimalLevel;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation allowing to alter the active {@link JsonView} for the serialization of a specific attribute.
 * 
 * For example, we might want to serialize an attribute only when its object is being serialized with at {@link DetailedLevel}. 
 * But we want the content of this attribute to be serialized in with at {@link MinimalLevel}. So we could proceed like this:
 * 
 * <pre>
 * <code> 
 * class Movie {
 * 
 *      public String getTitle() {...}
 * 
 *      {@literal @}JsonView(DetailedLevel.class)
 *      {@literal @}UseJsonView(MinimalLevel.class)
 *      public List{@literal <}Actor{@literal >} getActors() {...}
 * }
 * 
 * class Actor {
 * 
 *      public Name getName() {...}
 * 
 *      {@literal @}JsonView(DetailedLevel.class)
 *      public List{@literal <}Movie{@literal >} getAllMovies(){...}
 * }
 * </code>
 * </pre>
 * 
 * In this case, if we serialize a {@code Movie} object, only {@code movie.title} and {@code movie.actors[].name} would appeared.
 * 
 * @author Karl
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.FIELD })
public @interface UseJsonView {
    
    /**
     * @return Json view to apply for the serialization of the annotated element.
     */
    public Class<?> value();
    
    /**
     * @return true if this Json view should be applied only if another view is already active in the serialization process.
     */
    public boolean activeOnly() default false;
}
