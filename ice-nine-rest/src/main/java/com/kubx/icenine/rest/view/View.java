/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view;

/**
 * Marker class implemented by all view objects.
 * 
 * This interface does not declare any specific methods but will force a view class implementing
 * both {@link OutboundView} and {@link InboundView} to use the same generic type.
 * 
 * Having a view used for serializing a business type but deserializing another (or vice-versa) is
 * prohibited by design purpose only.
 * 
 * @author Karl
 */
public interface View <T> {
    
}
