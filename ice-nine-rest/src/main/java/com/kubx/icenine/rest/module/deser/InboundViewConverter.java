/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.module.deser;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.deser.std.StdDelegatingDeserializer;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;
import com.kubx.icenine.rest.view.InboundView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converts a view object bound in input to its business object.
 * 
 * This is registered with a {@link StdDelegatingDeserializer} for every view that has been bound in input
 * to a given business object type.
 * 
 * @author Karl
 */
public class InboundViewConverter implements Converter<InboundView, Object> {

    private static final Logger LOGGER = LoggerFactory.getLogger(InboundViewConverter.class);

    private final Class<?> businessClass;

    private final Class<? extends InboundView> viewClass;

    /**
     * Constructor
     * 
     * @param businessClass business type to convert to 
     * @param viewClass view type to convert from
     */
    public InboundViewConverter(Class<?> businessClass, Class<? extends InboundView> viewClass) {
        super();
        this.businessClass = businessClass;
        this.viewClass = viewClass;
    }

    @Override
    public Object convert(InboundView view) {
        Object value = null;
        
        if (view != null) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("Using view {} for deserialization of business object of type {}",
                        viewClass.getSimpleName(), businessClass.getSimpleName());
            }
            value = view.getDeserializedObject();
        }
        
        return value;
    }

    @Override
    public JavaType getInputType(TypeFactory typeFactory) {
        return typeFactory.constructType(viewClass);
    }

    @Override
    public JavaType getOutputType(TypeFactory typeFactory) {
        return typeFactory.constructType(businessClass);
    }
}
