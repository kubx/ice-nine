/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * View bound for the deserialization of a business object.
 *
 * The role a such view is to adapt the interface of a business object to the interface exposed to the client.
 * <p>
 * This establishes a clear separation of both models (the view and the business) by keeping serialization metadata, such as JSON annotations, in the view layer instead of
 * polluting the business layer with it.
 * <p>
 * Also, it defines the contract with the client applications that could remain static while the business evolves independently.
 *
 * @author Karl
 *
 * @param <T> type of business object
 */
public interface InboundView<T> extends View<T> {

    /**
     * Prepare the view for deserialization of the business object it represents.
     *
     * This is particularly useful for views that are bound both in input and output.
     *
     * Views should normally allocate here all resources needed to collect deserialized data and eventually build the business object.
     * 
     * @param businessClass type of object about to be deserialized
     */
    void prepareForDeserialization(Class<T> businessClass);

    /**
     * Get the business object that has been deserialized.
     *
     * {@link #prepareForDeserialization(java.lang.Class)} is always invoked prior to this call.
     *
     * @return deserialized value or null if nothing has been deserialized
     */
    @JsonIgnore
    T getDeserializedObject();
}
