/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.module.ser.level;

/**
 * Minimal level of verbosity.
 *
 * Information here should only include the minimal stuff (such as IDs...) so the object could be identified and retrieved
 * later.
 * <p>
 * There is no need to specify explicitly this level on the JSON fields, you could achieve the same by omitting the tag
 * using an active view for serialization.
 */
public class MinimalLevel {}
