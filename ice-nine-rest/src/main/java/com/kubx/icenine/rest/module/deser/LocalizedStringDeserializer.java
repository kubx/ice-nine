/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.module.deser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.kubx.icenine.util.LString;
import java.io.IOException;

/**
 *
 * @author Karl
 */
public class LocalizedStringDeserializer extends JsonDeserializer<LString> {

    @Override
    public LString deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        /*LocalizedString value = new LocalizedString();
        
        JsonNode node = jp.readValueAsTree();
        
        if (node.isValueNode()) {
            value.set(node.asText());
            
        } else {
            String text = node.get("text").asText();
            String language = node.get("lang").asText();
            value.setIn(text, language);
        }
        return value;*/
        return new LString(jp.getValueAsString());
    }
}
