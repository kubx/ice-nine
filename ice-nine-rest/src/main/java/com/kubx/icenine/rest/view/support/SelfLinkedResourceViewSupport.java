/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view.support;

import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Identifiable;

/**
 * Support class for resource views self-linking to the entity they are bound to.
 * <p>
 * If you override {@link #addExtraLinks(org.springframework.hateoas.EntityLinks)} to addLink up more links, make sure 
 to call {@code super.addLinks(entityLinks)} for not loosing this link.
 * 
 * @author Karl
 * 
 * @param <T> type of business object 
 */
public abstract class SelfLinkedResourceViewSupport<T extends Identifiable<?>> extends ResourceViewSupport<T> {
    
    @Override
    public void addExtraLinks(EntityLinks entityLinks) {
        addLink(entityLinks.linkToSingleResource((T) getAdaptee()));
    }    
}
