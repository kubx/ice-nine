/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.config;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.kubx.icenine.rest.module.RestModule;
import com.kubx.icenine.rest.view.registration.ViewRegistration;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * Configuration to import to enable Ice-Nine REST framework into the current application.
 * 
 * @author Karl
 */
@Configuration
@ComponentScan(basePackages = { "com.kubx.icenine.rest.view" })
@EnableEntityLinks
public class IceNineRestConfiguration {
    
    @Autowired(required = false)
    private List<ViewRegistration> viewRegistrations;
    
    @Bean
    public RestModule restModule() {//List<ViewRegistration> viewRegistrations) {
        return new RestModule(viewRegistrations);
    }
    
    @Bean
    public ObjectMapper objectMapper(RestModule restModule) {
        return Jackson2ObjectMapperBuilder.json()
                .featuresToEnable(MapperFeature.DEFAULT_VIEW_INCLUSION)
                .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .modulesToInstall(restModule, new JavaTimeModule())
                .build();
    }
}
