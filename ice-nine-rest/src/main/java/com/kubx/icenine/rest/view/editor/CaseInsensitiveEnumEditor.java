/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view.editor;

import java.beans.PropertyEditorSupport;
import java.util.Optional;

/**
 *
 * @author Karl
 */
public class CaseInsensitiveEnumEditor<E extends Enum> extends PropertyEditorSupport {
    
    private final Class<E> enumClass;
    
    private boolean lowerCaseOutput;

    public CaseInsensitiveEnumEditor(Class<E> enumClass) {
        this(enumClass, false);
    }

    public CaseInsensitiveEnumEditor(Class<E> enumClass, boolean lowerCaseOutput) {
        //this.enumClass = (Class<E>) ResolvableType.forClass(getClass()).resolveGeneric(0);
        this.enumClass = enumClass;
        this.lowerCaseOutput = lowerCaseOutput;
    }

    @Override
    public String getAsText() {
        return lowerCaseOutput ? getValue().toString().toLowerCase() : getValue().toString();
    }
    
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Optional.ofNullable(text).ifPresent((t) -> setValue(E.valueOf(enumClass, t.toUpperCase())));
    }    
}
