/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view.binding;

import com.kubx.icenine.rest.view.InboundView;
import com.kubx.icenine.rest.view.OutboundView;

/**
 * Gives binding information about a business class and its view(s).
 * 
 * @author Karl
 */
public class ViewBinding {
    
    private final Class<?> businessClass;
    
    private Class<? extends OutboundView> outboundViewClass;
    
    private Class<? extends InboundView> inboundViewClass;
    
    private Class<?> mixInClass;

    ViewBinding(Class<?> businessClass) {
        this.businessClass = businessClass;
    }

    public Class<?> getBusinessClass() {
        return businessClass;
    }

    public boolean isBoundInOutput() {
        return outboundViewClass != null;
    }
    
    public Class<? extends OutboundView> getOutboundViewClass() {
        return outboundViewClass;
    }

    public boolean isBoundInInput() {
        return inboundViewClass != null;
    }

    public Class<? extends InboundView> getInboundViewClass() {
        return inboundViewClass;
    }
    
    public boolean hasMixIn() {
        return mixInClass != null;
    }
    
    public Class<?> getMixInClass() {
        return mixInClass;
    }
     
    void setOutboundViewClass(Class<? extends OutboundView> outboundViewClass) {
        this.outboundViewClass = outboundViewClass;
    }

    void setInboundViewClass(Class<? extends InboundView> inboundViewClass) {
        this.inboundViewClass = inboundViewClass;
    }
    
    void setMixInClass(Class<?> mixInClass) {
        this.mixInClass = mixInClass;
    }
}
