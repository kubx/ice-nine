/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view;

import org.springframework.hateoas.EntityLinks;

/**
 * View bound for the serialization of a business object.
 *
 * The role a such view is to adapt the interface of a business object to the interface exposed to the client.
 * <p>
 * This establishes a clear separation of both models (the view and the business) by keeping serialization metadata, such as JSON annotations, in the view layer instead of
 * polluting the business layer with it.
 * <p>
 * Also, it defines the contract with the client applications that could remain static while the business evolves independently.
 *
 * @author Karl
 *
 * @param <T> type of business object
 */
public interface OutboundView<T> extends View<T> {

    /**
     * Prepare the view for the serialization of the business object passed in parameter.
     *
     * The view can safely keep a reference to the object if required.
     *
     * @param businessObject business object to serialize
     * @param entityLinks HATEOAS entity links, for building optional links
     */
    void prepareForSerialization(T businessObject, EntityLinks entityLinks);
}
