/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.module.ser;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ser.std.StdDelegatingSerializer;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;
import com.kubx.icenine.rest.view.OutboundView;
import com.kubx.icenine.rest.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.EntityLinks;

/**
 * Converts a business object to a compatible view bound in output.
 * 
 * This is registered with a {@link StdDelegatingSerializer} for every view that has been bound in 
 * output to a given business object type.
 * 
 * @author Karl
 */
public class OutboundViewConverter implements Converter<Object, OutboundView> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OutboundViewConverter.class);
        
    private final EntityLinks entityLinks;

    private final Class<?> businessClass;

    private final Class<? extends OutboundView> viewClass;

    /**
     * Constructor.
     * 
     * @param businessClass business type to convert to
     * @param viewClass view type to convert from
     * @param entityLinks reference to the {@link EntityLinks} bean
     */    
    public OutboundViewConverter(Class<?> businessClass, Class<? extends OutboundView> viewClass, EntityLinks entityLinks) {
        super();
        this.entityLinks = entityLinks;
        this.businessClass = businessClass;
        this.viewClass = viewClass;
    }

    @Override
    public OutboundView convert(Object value) {
        OutboundView view = null;

        if (value != null) {
            try {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("Using view {} for serialization of business object of type {}",
                            viewClass.getSimpleName(), businessClass.getSimpleName());
                }
                view = viewClass.newInstance();
                view.prepareForSerialization(value, entityLinks);

            } catch (ReflectiveOperationException e) {
                LOGGER.error("Fail to instantiate view {}, business object of type {} will not be serialized and be forced to null",
                        viewClass.getName(), businessClass.getName(), e);
            }
        }
        return view;
    }

    @Override
    public JavaType getInputType(TypeFactory typeFactory) {
        return typeFactory.constructType(businessClass);
    }

    @Override
    public JavaType getOutputType(TypeFactory typeFactory) {
        return typeFactory.constructType(viewClass);
    }

    protected Class<?> getBusinessClass() {
        return businessClass;
    }

    protected Class<? extends View> getViewClass() {
        return viewClass;
    }
}
