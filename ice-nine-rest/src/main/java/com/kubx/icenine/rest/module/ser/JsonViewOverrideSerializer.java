/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.module.ser;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.SerializerFactory;
import com.fasterxml.jackson.databind.util.NameTransformer;
import com.kubx.icenine.rest.module.annotation.UseJsonView;
import java.io.IOException;
import java.util.Iterator;

/**
 * Custom bean serializer that allows us to alter the active Json view during the serialization process.
 * 
 * Jackson does not allow to alter such property, that is selected (or not) at the very beginning of the serialization process (for example
 * by annotating a controller handler with {@link JsonView}). Some tricky stuff needs to be done to achieve our goal here, do not try
 * this at home!
 * 
 * @author Karl
 * @see UseJsonView
 */
public class JsonViewOverrideSerializer extends JsonSerializer<Object> {

    private JsonSerializer<Object> delegatee;

    private final Class<?> overrideView;
    
    private final boolean overrideActiveViewOnly;

    /**
     * Constructor.
     * 
     * @param delegatee the original instance of a bean serializer we delegate serialization to
     * @param overrideView the view to apply for the serialization of the bean
     * @param overrideActiveViewOnly true if the view should be applied only if another view is already active
     */
    public JsonViewOverrideSerializer(JsonSerializer<?> delegatee, Class<?> overrideView, boolean overrideActiveViewOnly) {
        this.delegatee = (JsonSerializer<Object>) delegatee;
        this.overrideView = overrideView;
        this.overrideActiveViewOnly = overrideActiveViewOnly;
    }

    @Override
    public JsonSerializer<?> getDelegatee() {
        return delegatee;
    }

    @Override
    public JsonSerializer<Object> replaceDelegatee(JsonSerializer<?> delegatee) {
        this.delegatee = (JsonSerializer<Object>) delegatee;
        return this;
    }

    @Override
    public JsonSerializer<Object> unwrappingSerializer(NameTransformer unwrapper) {
        return new JsonViewOverrideSerializer(delegatee.unwrappingSerializer(unwrapper), overrideView, overrideActiveViewOnly);
    }

    @Override
    public JsonSerializer<?> withFilterId(Object filterId) {
        return delegatee.withFilterId(filterId);
    }

    @Override
    public boolean isEmpty(SerializerProvider provider, Object value) {
        return delegatee.isEmpty(provider, value);
    }

    @Override
    public boolean usesObjectId() {
        return delegatee.usesObjectId();
    }

    @Override
    public boolean isUnwrappingSerializer() {
        return delegatee.isUnwrappingSerializer();
    }

    @Override
    public Iterator<PropertyWriter> properties() {
        return delegatee.properties();
    }

    @Override
    public void acceptJsonFormatVisitor(JsonFormatVisitorWrapper visitor, JavaType type) throws JsonMappingException {
        delegatee.acceptJsonFormatVisitor(visitor, type);
    }

    @Override
    public boolean isEmpty(Object value) {
        return delegatee.isEmpty(value);
    }

    @Override
    public Class<Object> handledType() {
        return delegatee.handledType();
    }

    @Override
    public void serializeWithType(Object value, JsonGenerator gen, SerializerProvider provider, TypeSerializer typeSer) throws IOException {
        SerializerProvider effectiveProvider = getEffectiveProvider(gen, provider);
        
        delegatee.serializeWithType(value, gen, effectiveProvider, typeSer);
    }

    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        SerializerProvider effectiveProvider = getEffectiveProvider(gen, provider);
        
        delegatee.serialize(value, gen, effectiveProvider);
    }

    private SerializerProvider getEffectiveProvider(JsonGenerator gen, SerializerProvider provider) throws JsonProcessingException {
        SerializerProvider effectiveProvider = provider;

        if ((provider.getActiveView() != null || !overrideActiveViewOnly) && overrideView != provider.getActiveView()) {            
            SerializerProviderModifier providerModifier = new SerializerProviderModifier();
            SerializerFactory serializerFactory =  ((ObjectMapper) gen.getCodec()).getSerializerFactory();                    
            
            effectiveProvider = providerModifier.modifyActiveView(overrideView, serializerFactory, provider);
        }

        return effectiveProvider;
    }
}
