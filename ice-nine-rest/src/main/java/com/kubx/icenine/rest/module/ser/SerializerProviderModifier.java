/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.module.ser;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class for modifying an instance of {@link SerializerProvider} during the serialization process.
 * 
 * @author Karl
 */
class SerializerProviderModifier {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonViewOverrideSerializer.class);

    private static final String ACTIVE_VIEW_MODIFIED_PROVIDERS_KEY = "activeViewModifiedProviders@" + SerializerProviderModifier.class.getName();

    /**
     * Modifies the active {@link JsonView} of the given provider.
     * 
     * In fact, the original provider will not be modified but "cloned", and it is its cloned that will be activated
     * with the given view.
     * 
     * @param activeView Json view to activate
     * @param serializerFactory serializer factory currently in context
     * @param provider serializer provider to modify
     * @return copy of a modified provider
     */
    SerializerProvider modifyActiveView(Class<?> activeView, SerializerFactory serializerFactory, SerializerProvider provider) {

        // Get the map of overridden providers from this provider attributes
        Map<Class<?>, SerializerProvider> modifiedProviders = Optional.ofNullable((Map) provider.getAttribute(ACTIVE_VIEW_MODIFIED_PROVIDERS_KEY)).orElseGet(() -> {
            Map<Class<?>, SerializerProvider> map = new HashMap();
            
            provider.setAttribute(ACTIVE_VIEW_MODIFIED_PROVIDERS_KEY, map);
            
            return map;
        });

        // Get or create a provider overriding the current one to use the selected Json view
        return modifiedProviders.computeIfAbsent(activeView, (k) -> {
            try {
                SerializerProvider modifiedProvider = ((DefaultSerializerProvider) provider).createInstance(provider.getConfig().withView(k), serializerFactory);
                
                modifiedProvider.setAttribute(ACTIVE_VIEW_MODIFIED_PROVIDERS_KEY, modifiedProviders);

                return modifiedProvider;

            } catch (ClassCastException e) {
                LOGGER.warn("Cannot override serializer provider, keeping original instance", e);
                return provider;
            }
        });
    }
}
