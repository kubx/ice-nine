/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.module;

import com.fasterxml.jackson.databind.Module.SetupContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDelegatingDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdDelegatingSerializer;
import com.kubx.icenine.rest.module.deser.InboundViewConverter;
import com.kubx.icenine.rest.module.deser.InboundViewInstantiator;
import com.kubx.icenine.rest.module.deser.LocalizedStringDeserializer;
import com.kubx.icenine.rest.module.ser.BeanSerializerModifier;
import com.kubx.icenine.rest.module.ser.LocalizedStringSerializer;
import com.kubx.icenine.rest.module.ser.OutboundViewConverter;
import com.kubx.icenine.rest.view.binding.ViewBinding;
import com.kubx.icenine.rest.view.registration.ViewRegistration;
import com.kubx.icenine.util.LString;
import com.kubx.icenine.util.testing.VisibleForTesting;
import java.lang.reflect.Modifier;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;

/**
 * Module to be registered with Jackson to add this library handlers.
 *
 * ObjectMapper used by JAX-RS must register this module using the {@link ObjectMapper#registerModule(com.fasterxml.jackson.databind.Module)} method so 
 * all custom handlers (serializers, deserializers, etc.) are made available.
 * <p>
 * It is important to initialize and add here any new handler that you are creating in this library.
 *
 * @author Karl
 */
public class RestModule extends SimpleModule {

    private final List<ViewRegistration> viewRegistrations;

    @Autowired
    private EntityLinks entityLinks;

    public RestModule(List<ViewRegistration> viewRegistrations) {
        this.viewRegistrations = viewRegistrations;
    }

    @Override
    public void setupModule(SetupContext context) {
        context.addBeanSerializerModifier(new BeanSerializerModifier());

        addSerializer(LString.class, new LocalizedStringSerializer());
        addDeserializer(LString.class, new LocalizedStringDeserializer());

        if (viewRegistrations != null) {
            setupViews();
        }        
        
        super.setupModule(context);
    }

    @VisibleForTesting(Modifier.PRIVATE)
    void setupViews() {        
        viewRegistrations.forEach((r) -> {
            
            r.getBindings().stream()
                    .filter(ViewBinding::isBoundInOutput)
                    .forEach((b) -> {
                        OutboundViewConverter converter = new OutboundViewConverter(b.getBusinessClass(), b.getOutboundViewClass(), entityLinks);
                        addSerializer(b.getBusinessClass(), new StdDelegatingSerializer(converter));
                    });

            r.getBindings().stream()
                    .filter(ViewBinding::isBoundInInput)
                    .forEach((b) -> {
                        InboundViewConverter converter = new InboundViewConverter(b.getBusinessClass(), b.getInboundViewClass());
                        addDeserializer(b.getBusinessClass(), new StdDelegatingDeserializer(converter));
                        addValueInstantiator(b.getInboundViewClass(), new InboundViewInstantiator(b.getBusinessClass(), b.getInboundViewClass()));
                    });

            r.getBindings().stream()
                    .filter(ViewBinding::hasMixIn)
                    .forEach((b) -> setMixInAnnotation(b.getBusinessClass(), b.getMixInClass()));
        });
    }
}
