/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view.registration;

import com.kubx.icenine.rest.view.binding.ViewBindingProvider;

/**
 * Container holding information about view resources of an application.
 * 
 * Application that want to use the REST view framework must provide an instance of this in their configuration.
 * This is normally by invoking {@link ViewRegistrar#done()} after all resource have been registered.
 * 
 * @author Karl
 */
public class ViewRegistration {
    
    private final ViewBindingProvider bindings;

    ViewRegistration(ViewBindingProvider bindings) {
        this.bindings = bindings;
    }
    
    /**
     * @return object providing bindings between business types and their view resources
     */
    public ViewBindingProvider getBindings() {
        return bindings;
    }
}
