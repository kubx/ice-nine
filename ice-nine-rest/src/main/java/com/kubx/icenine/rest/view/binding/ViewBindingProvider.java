/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view.binding;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Provide bindings issued from registered views.
 * 
 * @author Karl
 */
public class ViewBindingProvider {

    private final Map<Class<?>, ViewBinding> bindings = new HashMap<>();

    public boolean isBound(Class<?> businessClass) {
        return bindings.containsKey(businessClass);
    }
            
    public Optional<ViewBinding> getBinding(Class<?> businessClass) {
        return Optional.ofNullable(bindings.get(businessClass));
    }
    
    public ViewBinding getOrCreateBinding(Class<?> businessClass) {        
        return bindings.computeIfAbsent(businessClass, (k) -> new ViewBinding(k));
    }
    
    public Stream<ViewBinding> stream() {
        return bindings.values().stream();
    }
}
