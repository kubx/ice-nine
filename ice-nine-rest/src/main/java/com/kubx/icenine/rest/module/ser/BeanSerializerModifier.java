/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.module.ser;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanSerializer;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.kubx.icenine.rest.module.annotation.UseJsonView;
import java.io.IOException;

/**
 * Hooks some Ice-Nine features to the bean serializer factory process.
 *
 * We mainly use it to alter the standard contextualisation of the bean serializer, see {@link Contextualizer}.
 *
 * @author Karl
 */
public class BeanSerializerModifier extends com.fasterxml.jackson.databind.ser.BeanSerializerModifier {

    /**
     * Bean serializer that is only use for contextualization.
     *
     * Once a serializer is instantiate, it is normally contextualized to handle property-related attributes. It returns the original instance of a 
     * {@link BeanSerializer} if the property has nothing specific to handle or a custom bean serializer if it does.
     *
     * @author Karl
     */
    public static class Contextualizer extends JsonSerializer<Object> implements ContextualSerializer {

        private final JsonSerializer<?> beanSerializer;

        /**
         * Constructor.
         *
         * @param beanSerializer original instance of the bean serializer
         */
        public Contextualizer(JsonSerializer<?> beanSerializer) {
            this.beanSerializer = beanSerializer;
        }

        @Override
        public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
            JsonSerializer<?> serializer = beanSerializer;

            if (serializer instanceof ContextualSerializer) {
                serializer = ((ContextualSerializer) serializer).createContextual(prov, property);
            }

            if (property != null) {
                UseJsonView useJsonView = property.getAnnotation(UseJsonView.class);

                if (useJsonView != null) {
                    Class<?> jsonView = useJsonView.value();
                    boolean activeOnly = useJsonView.activeOnly();

                    serializer = new JsonViewOverrideSerializer(serializer, jsonView, activeOnly);
                }
            }

            return serializer;
        }

        @Override
        public void serialize(Object value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            throw new UnsupportedOperationException("This serializer is just used to contextualize its delegatee");
        }
    }

    @Override
    public JsonSerializer<?> modifySerializer(SerializationConfig config, BeanDescription beanDesc, JsonSerializer<?> serializer) {
        return new Contextualizer(serializer);
    }
}
