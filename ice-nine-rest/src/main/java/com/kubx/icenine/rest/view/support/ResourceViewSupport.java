/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view.support;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Identifiable;
import org.springframework.hateoas.Link;

/**
 * Support class for views bound in output that includes HATEOS links.
 * 
 * Such views supports serialization by default by keeping a simple reference to the business object
 * in its {@link #value} attribute accessible to getters methods of subclasses.
 * 
 * @author Karl
 * 
 * @param <T> type of business object 
 */
public abstract class ResourceViewSupport<T extends Identifiable<?>> extends OutboundViewSupport<T> {

    private Map<String, String> links;
    
    @Override
    public void prepareForSerialization(T value, EntityLinks entityLinks) {
        super.prepareForSerialization(value, entityLinks);
        
        this.links = new HashMap<>();
        
        addLink(entityLinks.linkToSingleResource(getAdaptee()));
        addExtraLinks(entityLinks);
    }
        
    @JsonProperty("_links")
    public Map<String, String> getLinks() {
        return links;
    }

    /**
     * Method called before serialization of this view to addLink HATEOS links to be included in this view.
     * 
     * @param entityLinks reference to the {@link EntityLinks} bean
     */
    protected void addExtraLinks(EntityLinks entityLinks) {}

    /**
     * Add a single link to this view
     * 
     * @param link 
     */
    protected void addLink(Link link) {
        links.put(link.getRel(), link.getHref());
    }
}
