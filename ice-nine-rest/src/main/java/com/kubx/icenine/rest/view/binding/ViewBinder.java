/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view.binding;

import com.kubx.icenine.rest.view.InboundView;
import com.kubx.icenine.rest.view.OutboundView;
import com.kubx.icenine.rest.view.View;
import com.kubx.icenine.rest.view.ViewMixIn;
import com.kubx.icenine.util.bean.CustomBeanScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Component;

/**
 * Binds a view object to its business class.
 *
 * @author Karl
 */
@Component
public class ViewBinder {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomBeanScanner.class);

    public ViewBinding bindView(Class<? extends View> viewClass, ViewBindingProvider viewBindings) {
        Class<?> businessClass = GenericTypeResolver.resolveTypeArgument(viewClass, View.class);

        ViewBinding binding = viewBindings.getOrCreateBinding(businessClass);

        if (OutboundView.class.isAssignableFrom(viewClass)) {
            if (binding.isBoundInOutput()) {
                LOGGER.warn("Both views {} and {} are bound in output to business type {}, only the former is kept",
                        viewClass.getSimpleName(), binding.getOutboundViewClass().getSimpleName(), businessClass.getName());
            }
            LOGGER.info("Binding outbound view {} to business type {}", viewClass.getSimpleName(), businessClass.getName());
            binding.setOutboundViewClass((Class<? extends OutboundView>) viewClass);
        }

        if (InboundView.class.isAssignableFrom(viewClass)) {
            if (binding.isBoundInInput()) {
                LOGGER.warn("Both views {} and {} are bound in input to business type {}, only the former is kept",
                        viewClass.getSimpleName(), binding.getInboundViewClass().getSimpleName(), businessClass.getName());
            }
            LOGGER.info("Binding inbound view {} to business type {}", viewClass.getSimpleName(), businessClass.getName());
            binding.setInboundViewClass((Class<? extends InboundView>) viewClass);
        }

        return binding;
    }

    public ViewBinding bindMixIn(Class<?> mixInClass, ViewBindingProvider viewBindings) {
        Class<?> businessClass = mixInClass.getAnnotation(ViewMixIn.class).value();

        ViewBinding binding = viewBindings.getOrCreateBinding(businessClass);

        if (binding.hasMixIn()) {
            LOGGER.warn("Both classes {} and {} are mix-in of business type {}, only the former is kept",
                    mixInClass.getSimpleName(), binding.getMixInClass().getSimpleName(), businessClass.getName());
        }
        LOGGER.info("Binding view mix-in {} to business type {}", mixInClass.getSimpleName(), businessClass.getName());
        binding.setMixInClass(mixInClass);

        return binding;
    }
}
