/*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.module.deser;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.ValueInstantiator;
import com.kubx.icenine.rest.view.InboundView;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A {@link ValueInstantiator} that allows us to invoke the {@link InboundView#prepareForDeserialization(java.lang.Class)} 
 * method before starting to deserialize a business object from its view.
 * 
 * @author Karl
 */
public class InboundViewInstantiator extends ValueInstantiator {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(InboundViewInstantiator.class);
    
    private final Class<?> businessClass;
    
    private final Class<? extends InboundView> viewClass;

    /**
     * Constructor.
     * 
     * @param businessClass business type handled by this view
     * @param viewClass view type to instantiate
     */
    public InboundViewInstantiator(Class<?> businessClass, Class<? extends InboundView> viewClass) {
        this.businessClass = businessClass;
        this.viewClass = viewClass;
    }
            
    @Override
    public String getValueTypeDesc() {
        return viewClass.getName();
    }

    @Override
    public Object createUsingDefault(DeserializationContext ctxt) throws IOException {                
        try {
            InboundView view = viewClass.newInstance();
            view.prepareForDeserialization(businessClass);                        
            return view;
        
        } catch (ReflectiveOperationException e) {
            LOGGER.error("Fail to allocate view {} for deserialization", viewClass.getName(), e);
            throw new IOException(e);
        }
    }

    @Override
    public boolean canCreateUsingDefault() {
        return true;
    }    
}
