 /*
 * Copyright 2015 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.rest.view.registration;

import com.kubx.icenine.util.bean.CustomBeanScanner;
import com.kubx.icenine.rest.view.View;
import com.kubx.icenine.rest.view.ViewMixIn;
import com.kubx.icenine.rest.view.binding.ViewBinder;
import com.kubx.icenine.rest.view.binding.ViewBindingProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Registers classes into the REST view framework.
 * 
 * Application that want to use this framework must provide a {@link ViewRegistration} in their configuration
 * by the intermediate of this class.
 * 
 * @author Karl
 */
@Component
@Scope("prototype")
public class ViewRegistrar {

    @Autowired
    private ViewBinder viewBinder;

    private final ViewBindingProvider viewBindings;
    
    public ViewRegistrar() {
        this.viewBindings = new ViewBindingProvider();
    }
    
    /**
     * Register a single class.
     * 
     * If the class inherits from the {@link View} interface, it will be bound to its business type
     * for serialization and/or deserialization.
     * 
     * If the class is annotated with {@link ViewMixIn}, it will be bound to its business type
     * for Jackson annotation mix-in.
     * 
     * Note that those two type of registration are exclusive, i.e. a class cannot be both registered
     * for both.
     * 
     * @param viewResourceClass class of the view to done
     * @return this instance
     */
    public ViewRegistrar registerClass(Class<?> viewResourceClass) {
        if (View.class.isAssignableFrom(viewResourceClass)) {
            viewBinder.bindView((Class<? extends View>) viewResourceClass, viewBindings);
            
        } else if (viewResourceClass.isAnnotationPresent(ViewMixIn.class)) {
            viewBinder.bindMixIn(viewResourceClass, viewBindings);
        }
        return this;
    }

    /**
     * Register all view resources recursively in the given package.
     * 
     * All classes implementing the {@link View} interface or annotated with {@link ViewMixIn} in the package 
     * will be automatically registered.
     * 
     * @param viewResourcePackage package to scan for view classes
     * @return this instance
     */
    public ViewRegistrar registerPackage(String viewResourcePackage) {
        CustomBeanScanner scanner = new CustomBeanScanner();

        scanner.includeInterfaces(View.class);
        scanner.includeAnnotations(ViewMixIn.class);

        scanner.scan(viewResourcePackage, this::registerClass);
        
        return this;
    }
    
    /**
     * Complete this registration, by returning an instance of {@link ViewRegistration} holding
     * all resources that might have been registered previously by this object.
     * 
     * @return view registration
     */
    public ViewRegistration done() {
        return new ViewRegistration(viewBindings);
    }
}
