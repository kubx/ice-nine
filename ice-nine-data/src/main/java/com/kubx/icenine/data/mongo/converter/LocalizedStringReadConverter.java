/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.icenine.data.mongo.converter;

import com.kubx.icenine.util.LString;
import com.mongodb.DBObject;
import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author lessardk
 */
public class LocalizedStringReadConverter implements Converter<DBObject, LString> {

    @Override
    public LString convert(DBObject dbo) {
        return new LString(dbo.toMap());
    }
}
