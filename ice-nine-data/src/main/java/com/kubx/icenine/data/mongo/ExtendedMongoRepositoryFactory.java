/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.icenine.data.mongo;

import java.io.Serializable;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.mapping.model.MappingException;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity;
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.MappingMongoEntityInformation;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory;
import org.springframework.data.repository.core.RepositoryInformation;

/**
 *
 * @author Karl
 */
class ExtendedMongoRepositoryFactory extends MongoRepositoryFactory {

    private MongoOperations mongoOperations;
    
    private final MappingContext<? extends MongoPersistentEntity<?>, MongoPersistentProperty> mappingContext;
    
    ExtendedMongoRepositoryFactory(MongoOperations mongoOperations) {
        super(mongoOperations);
        
        this.mongoOperations = mongoOperations;
        this.mappingContext = mongoOperations.getConverter().getMappingContext();
    }

    @Override
    protected Object getTargetRepository(RepositoryInformation information) {          
        if (!information.getRepositoryInterface().isAnnotationPresent(Collection.class)) {
            return super.getTargetRepository(information);
        }
        MongoEntityInformation<?, Serializable> entityInformation = getEntityInformation(information.getDomainType(), information);
        
        return getTargetRepositoryViaReflection(information, entityInformation, mongoOperations);
    }

    @SuppressWarnings("unchecked")
    private <T, ID extends Serializable> MongoEntityInformation<T, ID> getEntityInformation(Class<T> domainClass, RepositoryInformation information) {
        MongoPersistentEntity<?> entity = mappingContext.getPersistentEntity(domainClass);

        if (entity == null) {
            throw new MappingException(
                    String.format("Could not lookup mapping metadata for domain class %s!", domainClass.getName()));
        }
        
        String collectionName = null;
        
        if (information.getRepositoryInterface().isAnnotationPresent(Collection.class)) {
            collectionName = information.getRepositoryInterface().getAnnotation(Collection.class).value();
        }

        return new MappingMongoEntityInformation<T, ID>((MongoPersistentEntity<T>) entity, collectionName);
    }
}
