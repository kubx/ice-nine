/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.icenine.data.mongo.converter;

import com.kubx.icenine.util.LString;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author lessardk
 */
public class LocalizedStringWriteConverter implements Converter<LString, DBObject> {

    @Override
    public DBObject convert(LString s) {
        return new BasicDBObject(s.getTranslations());
    }
}
